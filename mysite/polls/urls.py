from django.conf.urls import patterns, url
from polls import views
from django.views.generic import DetailView, ListView
from polls.models import Poll
from django.utils import timezone


urlpatterns = patterns('',
    # ex: /polls/
    #url(r'^$', views.index, name='index'),
    url(r'^$',
    ListView.as_view(
        queryset=Poll.objects.filter(pub_date__lte=timezone.now) \
            .order_by('-pub_date')[:5],
        context_object_name='latest_poll_list',
        template_name='polls/index.html'),
    name='index'),

    # ex: /polls/5/
    url(r'^(?P<poll_id>\d+)/$', views.detail, name='detail'),

    # ex: /polls/5/results/
    url(r'^(?P<poll_id>\d+)/results/$', views.results, name='results'),

    # ex: /polls/5/vote/
    url(r'^(?P<poll_id>\d+)/vote/$', views.vote, name='vote'),

    )

